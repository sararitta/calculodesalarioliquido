package br.com.trab.entidades;

/**
 *
 * @author sara
 */
public class SimplesNacionalBean extends EmpregadorBean {

    public Double calculaDespesasFuncionario(Double salario) {
        Double totalDespesas = salario + FGTS(salario) + UmDozeAvosDoDecimoTerceiro(salario) + FGTSsemDecimoTerceiro(salario)
                + ferias(salario) + tercoDeFerias(salario) + FGTSsemFerias(salario) + FGTSsemTercoFerias(salario)
                + avisoPrevio(salario) + multaFGTS(salario);
        return totalDespesas;
    }
}
