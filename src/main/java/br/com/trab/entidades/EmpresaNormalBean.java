package br.com.trab.entidades;

/**
 *
 * @author sara
 */
public class EmpresaNormalBean extends EmpregadorBean {

    public Double INSS(Double salario) {
        Double inss = salario * (27.8d / 100d);
        return inss;
    }

    public Double INSSsemDecimoTerceiro(Double salario) {
        Double inssSemDecimoTerceiro = UmDozeAvosDoDecimoTerceiro(salario) * (27.8d / 100d);
        return inssSemDecimoTerceiro;
    }

    public Double INSSsemFerias(Double salario) {
        Double inssSemFerias = ferias(salario) * (27.8d / 100d);
        return inssSemFerias;
    }

    public Double INSSsemTercoFerias(Double salario) {
        Double inssSemTercoFerias = INSSsemFerias(salario) * (27.8d / 100d);
        return inssSemTercoFerias;
    }

    public Double calculaDespesasFuncionario(Double salario) {
        Double totalDespesas = salario + FGTS(salario) + UmDozeAvosDoDecimoTerceiro(salario) + FGTSsemDecimoTerceiro(salario)
                + ferias(salario) + tercoDeFerias(salario) + FGTSsemFerias(salario) + FGTSsemTercoFerias(salario)
                + avisoPrevio(salario) + multaFGTS(salario) + INSS(salario) + INSSsemDecimoTerceiro(salario)
                + INSSsemFerias(salario) + INSSsemTercoFerias(salario);

        return totalDespesas;
    }
}
