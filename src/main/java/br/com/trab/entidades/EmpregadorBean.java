package br.com.trab.entidades;

/**
 *
 * @author sara
 */
public class EmpregadorBean {

    public Double FGTS(Double salario) {
        Double FGTS = salario * (8d / 100d);
        return FGTS;
    }

    public Double UmDozeAvosDoDecimoTerceiro(Double salario) {
        Double umDozeAvosDoDecimoTerceiro = salario / 12d;
        return umDozeAvosDoDecimoTerceiro;
    }

    public Double ferias(Double salario) {
        Double ferias = salario / 12d;
        return ferias;
    }

    public Double tercoDeFerias(Double salario) {
        Double tercoDeFerias = ferias(salario) / (3d);
        return tercoDeFerias;
    }

    public Double avisoPrevio(Double salario) {
        Double avisoPrevio = salario * (8.33d / 100d);
        return avisoPrevio;
    }

    public Double multaFGTS(Double salario) {
        Double multaFGTS = FGTS(salario) + FGTSsemDecimoTerceiro(salario) + FGTSsemFerias(salario) + (FGTSsemTercoFerias(salario) * (50d / 100d));
        return multaFGTS;
    }

    public Double FGTSsemDecimoTerceiro(Double salario) {
        Double fgtsSemDecimoTerceiro = UmDozeAvosDoDecimoTerceiro(salario) * (8d / 100d);
        return fgtsSemDecimoTerceiro;
    }

    public Double FGTSsemFerias(Double salario) {
        Double fgtsSemFerias = UmDozeAvosDoDecimoTerceiro(salario) * (8d / 100d);
        return fgtsSemFerias;
    }

    public Double FGTSsemTercoFerias(Double salario) {
        Double fgtsSemTercoFerias = tercoDeFerias(salario) * (27.8d / 100d);
        return fgtsSemTercoFerias;
    }
}
