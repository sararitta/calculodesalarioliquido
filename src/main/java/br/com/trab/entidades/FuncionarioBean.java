package br.com.trab.entidades;

/**
 *
 * @author sara
 */
public class FuncionarioBean {

    public Double aliquotaINSS(Double salarioBruto) {
        Double aliquota = 0d;
        if (salarioBruto <= 1751.81) {
            aliquota = 8d;
        } else if (salarioBruto >= 1751.82 && salarioBruto <= 2919.72) {
            aliquota = 9d;
        } else if (salarioBruto >= 2919.73 && salarioBruto <= 5839.45) {
            aliquota = 11d;
        } else if (salarioBruto >= 5839.46) {
            aliquota = 0d;
        }
        return aliquota / 100;
    }

    public Object[] aliquotaIRRF(Double calculoBase) {
        Double aliquota = 0d;
        Double deducao = 0d;
        if (calculoBase <= 1093.98) {
            aliquota = 0d;
            deducao = 0d;
        } else if (calculoBase >= 1903.99 && calculoBase <= 2826.65) {
            aliquota = 7.5;
            deducao = 142.80;
        } else if (calculoBase >= 2826.66 && calculoBase <= 3751.05) {
            aliquota = 15d;
            deducao = 354.80;
        } else if (calculoBase >= 3751.06 && calculoBase <= 4664.68) {
            aliquota = 22.5;
            deducao = 636.13;
        } else if (calculoBase >= 4664.69) {
            aliquota = 27.5;
            deducao = 869.36;
        }
        return new Object[]{aliquota / 100, deducao};
    }

    public Double calculoBase(Double salarioBruto, Integer dependentes) {
        Double calculoBase;
        Double aliquotaINSS = this.aliquotaINSS(salarioBruto);
        if (aliquotaINSS > 0d) {
            calculoBase = salarioBruto - (salarioBruto * aliquotaINSS) - (dependentes * 189.59);
        } else {
            calculoBase = salarioBruto - 642.34 - (dependentes * 189.59);
        }
        return calculoBase;
    }

    public Double calculoIRRF(Double salarioBruto, Integer dependentes) {
        Double calculoIRRF = 0d;
        calculoIRRF = (this.calculoBase(salarioBruto, dependentes) * (Double) this.aliquotaIRRF(this.calculoBase(salarioBruto, dependentes))[0]) - (Double) this.aliquotaIRRF(this.calculoBase(salarioBruto, dependentes))[1];
        return calculoIRRF;
    }

    public Double salarioLiquido(Double salarioBruto, Integer dependentes) {
        Double salarioLiquido = 0d;
        Double inss = this.aliquotaINSS(salarioBruto) * salarioBruto;
        salarioLiquido = salarioBruto - inss - this.calculoIRRF(salarioBruto, dependentes);
        return salarioLiquido;
    }
}
