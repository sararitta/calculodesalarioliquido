package br.com.trab.legislacaoproject;

import br.com.trab.entidades.EmpresaNormalBean;
import br.com.trab.entidades.SimplesNacionalBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author sara
 */
@WebServlet(name = "CalculoEmprServlet", urlPatterns = {"/CalculoEmprServlet"})
public class CalculoEmprServlet extends HttpServlet {

    public CalculoEmprServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        Double salarioEmpr = Double.parseDouble(request.getParameter("salarioEmpregado"));
        String tipoEmpresa = request.getParameter("radio");

        Double resultado;
        if (tipoEmpresa.equalsIgnoreCase("normal")) {
            EmpresaNormalBean empresaNormalBean = new EmpresaNormalBean();
            resultado = empresaNormalBean.calculaDespesasFuncionario(salarioEmpr);
        } else {
            SimplesNacionalBean simplesNacionalBean = new SimplesNacionalBean();
            resultado = simplesNacionalBean.calculaDespesasFuncionario(salarioEmpr);
        }

        DecimalFormat df = new DecimalFormat("#0.00");
        String sResultado = df.format(resultado);

        request.setAttribute("resultado", sResultado);
        getServletConfig().getServletContext().getRequestDispatcher("/respostaSalarioFuncionario.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
