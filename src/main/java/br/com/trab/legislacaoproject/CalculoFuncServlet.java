package br.com.trab.legislacaoproject;

import br.com.trab.entidades.FuncionarioBean;
import java.io.IOException;
import java.text.DecimalFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author sara
 */
@WebServlet(name = "CalculoFuncServlet", urlPatterns = {"/CalculoFuncServlet"})
public class CalculoFuncServlet extends HttpServlet {

    public CalculoFuncServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        Double salarioFunc = Double.parseDouble(request.getParameter("salarioFunc"));
        Integer dependentes = Integer.parseInt(request.getParameter("dependentes"));

        FuncionarioBean funcionario = new FuncionarioBean();
        Double resultado = funcionario.salarioLiquido(salarioFunc, dependentes);
        DecimalFormat df = new DecimalFormat("#0.00");
        String sResultado = df.format(resultado);
        
        request.setAttribute("resultado", sResultado);       
        getServletConfig().getServletContext().getRequestDispatcher("/respostaSalarioLiquido.jsp").forward(request, response);        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
