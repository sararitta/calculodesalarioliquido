<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <style>
        html {
            background-image: url(img/fotoTurma.jpg);   
            background-size: 1550px;
        }

        .tabs-container {
            position: relative;
            height: 290px;
            max-width: 30%;
            margin: 150px auto;
        }
        .tabs-container p {
            margin: 0;
            padding: 0;
        }
        .tabs-container:after {
            content: '.';
            display: block;
            clear: both;
            height: 0;
            font-size: 0;
            line-height: 0;
            visibility: none;
        }

        input.tabs {
            display: none;
        }
        input.tabs + label + div {
            width: 86%;
            opacity: 0;
            position: absolute;
            background: #eee;
            top: 70px;
            left: 10;
            height: 230px;
            padding: 10px;
            z-index: -1;
            transition: opacity ease-in-out .3s;
        }
        input.tabs:checked + label + div {
            opacity: 1;
            z-index: 1000;
        }

        input.tabs + label {
            line-height: 40px;
            padding: 0 20px;
            float: left;
            background: #444;
            color: #fff;
            cursor: pointer;
            transition: background ease-in-out .3s;
        }
        input.tabs:checked + label {
            color: #000;
            background: #eee;
        }
        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type=number], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #45a049;
        }

        a {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 163px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        a:hover {
            background-color: #45a049;
        }

        a:link {
            text-decoration:none;
        }

        a:visited {
            text-decoration:none;
        }

        a:hover {
            text-decoration:underline;
        }

        div {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;        
            opacity: 0.85;
        }
        .formFunc {
            top: 50px;
        }
        .formEmpreg {
            top: 50px;
        }
        .container {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 16px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .container input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
        }

        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 20px;
            width: 20px;
            background-color: #eee;
            border-radius: 50%;
        }

        .container:hover input ~ .checkmark {
            background-color: #ccc;
        }

        .container input:checked ~ .checkmark {
            background-color: #22AB00;
        }

        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        .container input:checked ~ .checkmark:after {
            display: block;
        }

        .container .checkmark:after {
            top: 7px;
            left: 7px;
            width: 6px;
            height: 6px;
            border-radius: 50%;
            background: white;
        }
    </style> 
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculo de Salário</title>
    </head>
    <body>
        <div class="tabs-container">
            <input disabled="true" type="radio" name="tabs" class="tabs" id="tab1">
            <label for="tab1">Funcionário</label>            
            <input type="radio" name="tabs" class="tabs" id="tab2" checked>
            <label for="tab2">Empregado</label>            
            <div>
                <form action="CalculoEmprServlet" method="get" class="formFunc">
                    <label for="fsalfunc">Despesas Totais (R$)</label><br/><br/>
                    <input type="text" disabled id="result" name="result" value="<%out.print(request.getAttribute("resultado").toString());%>">                                                            
                    <br/>
                    <br/>
                    <br/>
                    <a href='paginaInicial.html'>Voltar</a>
                </form>
            </div>
        </div>        
    </body>
</html>
